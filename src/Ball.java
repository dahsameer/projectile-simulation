import java.math.BigDecimal;
import java.math.RoundingMode;

public class Ball {
    private double x;
    private double y;
    private double gravity;
    private double speed;
    private double speedX;
    private double speedY;
    private double angle;
    private double deltaTime;
    private boolean isFired;

    public Ball(){
        x = 10;
        y = 400;
        gravity = 30;
        speed = 800;
        angle = 45;
        deltaTime = 0.001;
        speedX = speed * Math.cos(Math.toRadians(-this.angle));
        speedY = speed * Math.sin(Math.toRadians(-this.angle));
        isFired = false;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void updateLocation(){
        if(isFired) {
            this.x += this.speedX * this.deltaTime;
            this.y += this.speedY * this.deltaTime;
            if(y < 470){
                this.speedY += this.gravity * 0.5;
            }
            if (y > 470) {
                y = 470;
                speedX = 0;
                speedY = 0;
            }
        }
    }

    public void increaseSpeed(){
        this.speed += 10;
    }
    public void calculateSpeed(){
        speedX = speed * Math.cos(Math.toRadians(-this.angle));
        speedY = speed * Math.sin(Math.toRadians(-this.angle));
    }
    public void fired(){
        this.isFired = true;
    }
    public double getSpeed(){
        return truncate(this.speed);
    }
    public double getSpeedX(){
        return truncate(this.speedX);
    }
    public double getSpeedY(){
        return truncate(this.speedY);
    }
    public void increaseAngle(){
        this.angle++;
    }
    public void decreaseAngle(){
        this.angle--;
    }
    public double getAngle(){
        return this.angle;
    }
    public double getRange(){
        return truncate(this.x-10);
    }
    public void flipGravity(){
        if(gravity==30){
            gravity = 10;
        }
        else{
            gravity = 30;
        }
    }
    public double getHeight(){
        return truncate(500-this.y-30);
    }
    public double truncate(double x){
        return BigDecimal.valueOf(x)
                .setScale(3, RoundingMode.HALF_UP)
                .doubleValue();
    }
}