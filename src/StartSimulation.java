import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;


public class StartSimulation extends JFrame implements KeyListener {

    ArrayList<Ball> balls = new ArrayList<Ball>();
    private boolean ballMade;
    private String gravity;

    public StartSimulation(){
        this.setTitle("Projectile Simulation");
        this.setSize(1000,500);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.addKeyListener(this);
        this.gravity = "Earth";
    }

    public static void main(String[] args) throws InterruptedException {
        StartSimulation s = new StartSimulation();
        s.ballMade = false;
        while(true){
            Thread.sleep(60);
            s.repaint();
            for(int i=0; i<s.balls.size(); i++){
                s.balls.get(i).updateLocation();
            }
        }
    }

    public void paint(Graphics g){
        g.setColor(Color.DARK_GRAY);
        g.fillRect(0,0,1000,500);
        for(int i=0; i<this.balls.size(); i++){
            g.setColor(Color.MAGENTA);
            g.fillOval((int)this.balls.get(i).getX(),(int) this.balls.get(i).getY(),20,20);
            g.setColor(Color.YELLOW);
            g.drawOval((int)this.balls.get(i).getX(),(int) this.balls.get(i).getY(),20,20);
            g.setColor(Color.WHITE);
            g.drawString("Speed: "+this.balls.get(this.balls.size()-1).getSpeed(), 50,50);
            g.drawString("SpeedX: "+this.balls.get(this.balls.size()-1).getSpeedX(), 50,60);
            g.drawString("SpeedY: "+this.balls.get(this.balls.size()-1).getSpeedY(), 50,70);
            g.drawString("Angle: "+this.balls.get(this.balls.size()-1).getAngle(), 50,80);
            g.drawString("Range: "+this.balls.get(this.balls.size()-1).getRange(), 50,90);
            g.drawString("Gravity: "+this.gravity, 50,100);
            g.drawString("Height: "+this.balls.get(this.balls.size()-1).getHeight(), 50,110);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int keyCode = e.getKeyCode();
        if(ballMade) {
            if (keyCode == KeyEvent.VK_SPACE) {
                this.balls.get(this.balls.size() - 1).increaseSpeed();
                //System.out.println(this.balls.get(this.balls.size()-1).getSpeed());
            }
            else if(keyCode == KeyEvent.VK_UP){
                this.balls.get(this.balls.size()-1).increaseAngle();
            }
            else if(keyCode == KeyEvent.VK_DOWN){
                this.balls.get(this.balls.size()-1).decreaseAngle();
            }
            else if(keyCode == KeyEvent.VK_G){
                this.balls.get(this.balls.size()-1).flipGravity();
                if(this.gravity == "Earth"){
                    this.gravity = "Moon";
                }
                else{
                    this.gravity = "Earth";
                }
            }
        }
        else{
            balls.add(new Ball());
            ballMade = true;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int keyCode = e.getKeyCode();
        if(keyCode == KeyEvent.VK_SPACE){
            ballMade = false;
            this.balls.get(this.balls.size()-1).fired();
            this.balls.get(this.balls.size()-1).calculateSpeed();
        }
    }
}
