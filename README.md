# Projectile Simulation

## A Mini project for Simulation and Modeling

This project was made in IDEA. It can be pulled and run directly from IDEA.

## Usage:
* The screen shows projectile only after pressing any one of the keys.
* Up and Down arrow can be used to change the angle.
* The key 'G' can be used to toggle between gravity in Earth and Moon.
* Spacebar fires the projectile.
* Long pressing the spacebar increases the speed of the projectile.
